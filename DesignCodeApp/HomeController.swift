//
//  HomeController.swift
//  DesignCodeApp
//
//  Created by Simon Quach on 2018-07-14.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import AVKit

class HomeController: UIViewController {
  struct CellIdentifiers {
    static let sectionCell = "SectionCell"
  }

  struct SegueIdentifiers {
    static let homeToSection = "HomeToSection"
  }

  @IBOutlet weak var scrollView: UIScrollView!
  // MARK: heroView Properties
  @IBOutlet weak var heroView: UIView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var deviceImageView: UIImageView!
  @IBOutlet weak var playVisualEffectView: UIVisualEffectView!
  @IBOutlet weak var backgroundImageView: UIImageView!
  // MARK: bookView Properties
  @IBOutlet weak var bookView: UIView!

  @IBOutlet weak var chapterCollectionView: UICollectionView!

  var isStatusBarHidden = false

  // MARK: IBAction Methods
  @IBAction func playButtonTapped(_ sender: Any) {
    let urlString = "https://player.vimeo.com/external/235468301.hd.mp4?s=e852004d6a46ce569fcf6ef02a7d291ea581358e&profile_id=175"
    guard let url = URL(string: urlString) else { return }
    let player = AVPlayer(url: url)
    let playerController = AVPlayerViewController()
    playerController.player = player
    present(playerController, animated: true) {
      player.play()
    }
    
  }

  // MARK: Override Methods
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setStatusBarHidden(false)
    self.navigationController?.navigationBar.barStyle = UIBarStyle.black
    self.navigationController?.navigationBar.isTranslucent = true
    self.navigationController?.navigationBar.isOpaque = false
    self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 50)]

  }
  override func viewDidLoad() {
    super.viewDidLoad()
    setupLayout()
  }

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return UIStatusBarStyle.lightContent
  }

  override var prefersStatusBarHidden: Bool {
    return isStatusBarHidden
  }

  override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
    return UIStatusBarAnimation.slide
  }

  // MARK: - Navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == SegueIdentifiers.homeToSection {
      let sectionController = segue.destination as! SectionViewController
      // In the performSegue method, we pass in indexPath as the sender
      let indexPath = sender as! IndexPath
      let section = sections[indexPath.item]
      sectionController.sections = sections
      sectionController.section = section
      sectionController.indexPath = indexPath
      setStatusBarHidden(true)
    }
  }
}

// MARK: - Setup methods
private extension HomeController {
  func setupLayout() {
//    setStatusBarBackgroundColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5))
    navigationController?.setNavigationBarHidden(true, animated: false)

    // Setup bar button items
    // This is also done in the storyboard to showcase both ways (programmatic and storyboard)
    let loginItem = UIBarButtonItem(title: "Login", style: .done, target: self, action: #selector(handleLogin))
    navigationItem.rightBarButtonItem = loginItem
    let logoItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Logo-DesignCode"), style: .plain, target: nil, action: nil)
    navigationItem.leftBarButtonItem = logoItem
    let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 128, height: 28))
    titleView.backgroundColor = .clear
    let buyButton = UIButton(type: .system)
    buyButton.setTitle("Get The Book", for: .normal)
    buyButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
    buyButton.setTitleColor(.white, for: .normal)
    buyButton.backgroundColor = .blue
    print(titleView.frame)
//    titleView.addSubview(buyButton)
    buyButton.frame = titleView.frame
    navigationItem.titleView = titleView

    scrollView.delegate = self
    chapterCollectionView.delegate = self
    chapterCollectionView.dataSource = self

    let invisible: CGFloat = 0
    let visible: CGFloat = 1

    titleLabel.alpha = invisible
    deviceImageView.alpha = invisible
    playVisualEffectView.alpha = invisible

    UIView.animate(withDuration: 1) {
      self.titleLabel.alpha = visible
      self.deviceImageView.alpha = visible
      self.playVisualEffectView.alpha = visible
    }
  }

  func setStatusBarBackgroundColor(_ color: UIColor) {
    guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
    statusBar.backgroundColor = color
  }

  /// Show or hide the status bar with a simple animation
  /// - Parameter isHidden: Determines whether the status bar will be hidden
  func setStatusBarHidden(_ isHidden: Bool) {
    isStatusBarHidden = isHidden
    UIView.animate(withDuration: 0.5) {
      self.setNeedsStatusBarAppearanceUpdate()
    }
  }
}

// MARK: - Handler Methods
private extension HomeController {
  @objc func handleLogin() {
    print("handleLogin")
  }
}

// MARK: - Helper Methods
private extension HomeController {
  func translateYPosition(with offsetY: CGFloat, by value: CGFloat = 3) -> CGAffineTransform {
    // Divide y offset by negative number for speed
    // The larger the number, the slower the object moves
    // y offset will be positive in the transform (two negatives = positive)
    // i.e. if y position is -10, dividing by -5(speed) will move object down by 2pt vs -10 / -2 = 5pt down
    // 2pt down = slower than 5pt down
    return CGAffineTransform(translationX: 0, y: -offsetY / value)
  }
}

// MARK: - UIScrollViewDelegate
extension HomeController: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if let collectionView = scrollView as? UICollectionView {
      for cell in collectionView.visibleCells as! [SectionCell] {
        guard let indexPath = collectionView.indexPath(for: cell) else { return }
        guard let attributes = collectionView.layoutAttributesForItem(at: indexPath) else { continue }
        let cellFrame = collectionView.convert(attributes.frame, to: self.view)
//        print(indexPath.item, cellFrame)
        // Testing speed from scrolling (lower is faster)
//        let divideBy: CGFloat = indexPath.item % 2 == 0 ? 1 : 5
        let translationX = cellFrame.origin.x / 5
        cell.coverImageView.transform = CGAffineTransform(translationX: translationX, y: 0)

        cell.layer.transform = animateCell(with: cellFrame)
      }
    }

    let offsetY = scrollView.contentOffset.y
//    print(offsetY)
    // Measures how fast the user scroll

//    print(scrollView.panGestureRecognizer.velocity(in: self.view))

    if offsetY < 0 {
      // Fixes the heroView to be at the top
      // Moves the heroView y position to the same the offset
      heroView.transform = CGAffineTransform(translationX: 0, y: offsetY)
      playVisualEffectView.transform = translateYPosition(with: offsetY, by: 3)
      titleLabel.transform = translateYPosition(with: offsetY, by: 3)
      deviceImageView.transform = translateYPosition(with: offsetY, by: 4)
//      backgroundImageView.transform = translateYPosition(with: offsetY, by: 5)
    }

    let navigationBarIsHidden = offsetY <= 0
    navigationController?.setNavigationBarHidden(navigationBarIsHidden, animated: true)

  }
}

// MARK: - UICollectionViewDataSource
extension HomeController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return sections.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.sectionCell, for: indexPath) as! SectionCell
    let section = sections[indexPath.item]
    cell.section = section

    cell.layer.transform = animateCell(with: cell.frame)
    return cell
  }
}

// MARK: - UICollectionViewDelegate
extension HomeController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    performSegue(withIdentifier: SegueIdentifiers.homeToSection, sender: indexPath)
  }

  // Overrides the values set from storyboard (under Collection View -> Cell Size and Collection View Cell -> Size)
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: 304, height: 248)
  }

  // Overrides the values set from storyboard (under section insets)
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets.init(top: 0, left: 20, bottom: 0, right: 20)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
}

