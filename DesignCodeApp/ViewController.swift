//
//  ViewController.swift
//  DesignCodeApp
//
//  Created by Simon Quach on 2018-07-14.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import AVKit

class ViewController: UIViewController {
  struct CellIdentifiers {
    static let sectionCell = "SectionCell"
  }

  struct SegueIdentifiers {
    static let homeToSection = "HomeToSection"
  }

  @IBOutlet weak var scrollView: UIScrollView!
  // MARK: heroView Properties
  @IBOutlet weak var heroView: UIView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var deviceImageView: UIImageView!
  @IBOutlet weak var playVisualEffectView: UIVisualEffectView!
  @IBOutlet weak var backgroundImageView: UIImageView!
  // MARK: bookView Properties
  @IBOutlet weak var bookView: UIView!

  @IBOutlet weak var chapterCollectionView: UICollectionView!

  var isStatusBarHidden = false

  let sections = [
    [
      "title": "Learn iOS 11 Design",
      "caption": "A complete guide to designing for iOS 11",
      "body": "While the flat design has become universal over the past 5 years, it wasn’t so common before iOS 7. It was the shift that shaped the design landscape. But to say that it hasn’t evolved would be inaccurate. iOS design has adapted to the bigger screens. What started as the ultimate opposite of hyper-realistic designs that preceded it, flat design is now much more nuanced, giving way to gradients, drop shadows and cards.",
      "image": "ios11"
    ],
    [
      "title": "Designing for iPhone X",
      "caption": "Guidelines to designing for iOS 11",
      "body": "iOS 11 marks the introduction of the iPhone X, a much taller iPhone that has virtually no bezel. The 5.8-inch OLED screen is larger than the iPhone 8 Plus’s 5.5-inch, yet the body size is about the same as the iPhone 8. For designers, this means more freedom in our canvas.",
      "image": "ios11-iphone-x"
    ],
    [
      "title": "Design for iPad",
      "caption": "How bigger screens affect your design",
      "body": "Designing for the iPad isn’t as simple as flipping a switch and just making everything bigger. A larger screen provides a real opportunity to present more content while respecting some basic rules in navigation, typography and visual hierarchy. Don’t treat the iPad as just a big iPhone. Instead, treat it more like a desktop computer with touch capabilities. In other words, your users can be more productive, see more content and perform tasks faster like typing, and drag-and-drop and multi-tasking.",
      "image": "ios11-ipad"
    ],
    [
      "title": "Design for Apple Watch",
      "caption": "Designing for people on the go",
      "body": "Apple Watch was introduced on April 24, 2015, and it was highly anticipated by developers, designers, and the media. It was truly the first wearable that broke every expectation, placing Apple as the number one watch manufacturer in the world.",
      "image": "ios11-watch"
    ],
    [
      "title": "Learn Colors",
      "caption": "How to work with colors",
      "body": "Colors are difficult to master because it’s really easy to go overboard. When we design, we have a tendency to over-design. For colors, we tend to use competing colors that distract and just feels completely unnatural. What I can recommend is to simply stick to the basics and temper your use of colors by focusing on its utility and pleasantness. When in doubt, use colors only to draw attention to a button or element of importance.",
      "image": "ios11-colors"
    ]
  ]



  // MARK: IBAction Methods
  @IBAction func playButtonTapped(_ sender: Any) {
    let urlString = "https://player.vimeo.com/external/235468301.hd.mp4?s=e852004d6a46ce569fcf6ef02a7d291ea581358e&profile_id=175"
    guard let url = URL(string: urlString) else { return }
    let player = AVPlayer(url: url)
    let playerController = AVPlayerViewController()
    playerController.player = player
    present(playerController, animated: true) {
      player.play()
    }
    
  }

  // MARK: Override Methods
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setStatusBarHidden(false)
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    setupLayout()
  }

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return UIStatusBarStyle.lightContent
  }

  override var prefersStatusBarHidden: Bool {
    return isStatusBarHidden
  }

  override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
    return UIStatusBarAnimation.slide
  }

  // MARK: - Navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == SegueIdentifiers.homeToSection {
      let sectionController = segue.destination as! SectionViewController
      // In the performSegue method, we pass in indexPath as the sender
      let indexPath = sender as! IndexPath
      let section = sections[indexPath.item]
      sectionController.sections = sections
      sectionController.section = section
      sectionController.indexPath = indexPath
      setStatusBarHidden(true)
    }
  }
}

// MARK: - Setup methods
private extension ViewController {
  func setupLayout() {
    setStatusBarBackgroundColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5))
    scrollView.delegate = self
    chapterCollectionView.delegate = self
    chapterCollectionView.dataSource = self

    let invisible: CGFloat = 0
    let visible: CGFloat = 1

    titleLabel.alpha = invisible
    deviceImageView.alpha = invisible
    playVisualEffectView.alpha = invisible

    UIView.animate(withDuration: 1) {
      self.titleLabel.alpha = visible
      self.deviceImageView.alpha = visible
      self.playVisualEffectView.alpha = visible
    }
  }

  func setStatusBarBackgroundColor(_ color: UIColor) {
    guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
    statusBar.backgroundColor = color
  }

  /// Show or hide the status bar with a simple animation
  /// - Parameter isHidden: Determines whether the status bar will be hidden
  func setStatusBarHidden(_ isHidden: Bool) {
    isStatusBarHidden = isHidden
    UIView.animate(withDuration: 0.5) {
      self.setNeedsStatusBarAppearanceUpdate()
    }
  }
}

// MARK: - Helper Methods
private extension ViewController {
  func translateYPosition(with offsetY: CGFloat, by value: CGFloat = 3) -> CGAffineTransform {
    // Divide y offset by negative number for speed
    // The larger the number, the slower the object moves
    // y offset will be positive in the transform (two negatives = positive)
    // i.e. if y position is -10, dividing by -5(speed) will move object down by 2pt vs -10 / -2 = 5pt down
    // 2pt down = slower than 5pt down
    return CGAffineTransform(translationX: 0, y: -offsetY / value)
  }

  func animateCell(with cellFrame: CGRect) -> CATransform3D {
    // Ch.3 3D Animation
    // Change how the section scrolling looks

    // Rotates the cells inwards
    let angleFromX = Double((-cellFrame.origin.x) / 10)
    let angle = CGFloat((angleFromX * Double.pi) / 180)

    var transform = CATransform3DIdentity
    transform.m34 = -1 / 1000
    let rotation = CATransform3DRotate(transform, angle, 0, 1, 0)

    // Scale the size of cell (make it look big and small)
    // Set a limit
    // As the origin moves over to the left more, scaleFromX becomes closer to 1 (normal size looking cell)
    var scaleFromX = (1000 - (cellFrame.origin.x - 200)) / 1000
    let scaleMax: CGFloat = 1
    let scaleMin: CGFloat = 0.6
    if scaleFromX > scaleMax {
      scaleFromX = scaleMax
    }
    if scaleFromX < scaleMin {
      scaleFromX = scaleMin
    }

    let scale = CATransform3DScale(CATransform3DIdentity, scaleFromX, scaleFromX, 1)

    return CATransform3DConcat(rotation, scale)
  }
}

// MARK: - UIScrollViewDelegate
extension ViewController: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if let collectionView = scrollView as? UICollectionView {
      for cell in collectionView.visibleCells as! [SectionCell] {
        guard let indexPath = collectionView.indexPath(for: cell) else { return }
        guard let attributes = collectionView.layoutAttributesForItem(at: indexPath) else { continue }
        let cellFrame = collectionView.convert(attributes.frame, to: self.view)
//        print(indexPath.item, cellFrame)
        // Testing speed from scrolling (lower is faster)
//        let divideBy: CGFloat = indexPath.item % 2 == 0 ? 1 : 5
        let translationX = cellFrame.origin.x / 5
        cell.coverImageView.transform = CGAffineTransform(translationX: translationX, y: 0)

        cell.layer.transform = animateCell(with: cellFrame)
      }
    }

    let offsetY = scrollView.contentOffset.y
    // Measures how fast the user scroll

//    print(scrollView.panGestureRecognizer.velocity(in: self.view))

    if offsetY < 0 {
      // Fixes the heroView to be at the top
      // Moves the heroView y position to the same the offset
      heroView.transform = CGAffineTransform(translationX: 0, y: offsetY)
      playVisualEffectView.transform = translateYPosition(with: offsetY, by: 3)
      titleLabel.transform = translateYPosition(with: offsetY, by: 3)
      deviceImageView.transform = translateYPosition(with: offsetY, by: 4)
//      backgroundImageView.transform = translateYPosition(with: offsetY, by: 5)
    }
  }
}

// MARK: - UICollectionViewDataSource
extension ViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return sections.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.sectionCell, for: indexPath) as! SectionCell
    let section = sections[indexPath.item]
    cell.section = section

    cell.layer.transform = animateCell(with: cell.frame)
    return cell
  }
}

// MARK: - UICollectionViewDelegate
extension ViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    performSegue(withIdentifier: SegueIdentifiers.homeToSection, sender: indexPath)
  }

  // Overrides the values set from storyboard (under Collection View -> Cell Size and Collection View Cell -> Size)
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: 304, height: 248)
  }

  // Overrides the values set from storyboard (under section insets)
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsetsMake(0, 20, 0, 20)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
}

