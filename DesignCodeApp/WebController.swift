//
//  WebController.swift
//  DesignCodeApp
//
//  Created by Simon Quach on 2018-09-05.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import WebKit

class WebController: UIViewController {

  @IBOutlet weak var webView: WKWebView!
  var urlString: String!

  @IBAction func doneButtonTapped(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }

  @IBAction func reloadButtonTapped(_ sender: Any) {
    webView.reload()
  }

  @IBAction func goBackButtonTapped(_ sender: Any) {
    webView.goBack()
  }

  @IBAction func goForwardButtonTapped(_ sender: Any) {
    webView.goForward()
  }

  @IBAction func actionButtonTapped(_ sender: Any) {
    let activityItems = [urlString] as! [String]
    let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
    activityController.excludedActivityTypes = [.postToFacebook]
    present(activityController, animated: true, completion: nil)
  }

  @IBAction func safariButtonTapped(_ sender: Any) {
    guard let url = URL(string: urlString) else { return }
    UIApplication.shared.open(url)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    guard let url = URL(string: urlString) else { return }
    let request = URLRequest(url: url)
    webView.load(request)

    webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
  }

  deinit {
    webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress))
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    guard let url = URL(string: urlString) else { return }
    let request = URLRequest(url: url)
    webView.load(request)
  }

  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    if keyPath == "estimatedProgress" {
      if webView.estimatedProgress == 1.0 {
        navigationItem.title = webView.title
      } else {
        navigationItem.title = "Loading..."
      }
    }
  }

}
