//
//  BookmarksController.swift
//  DesignCodeApp
//
//  Created by Simon Quach on 2018-08-20.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class BookmarksController: UITableViewController {
  struct CellIdentifiers {
    static let bookmarkCell = "BookmarkCell"
  }
  var bookmarks: [[String: String]] = allBookmarks

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    tableView.reloadData()
  }
}

// MARK: Setup Methods
private extension BookmarksController {
  
}

extension BookmarksController {
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return bookmarks.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.bookmarkCell, for: indexPath) as! BookmarkCell
    let bookmark = bookmarks[indexPath.row]
    cell.chapterTitleLabel.text = bookmark["section"]!.uppercased()
    cell.titleLabel.text = bookmark["part"]
    cell.bodyLabel.text = bookmark["content"]
    cell.chapterNumberLabel.text = bookmark["chapter"]
    cell.badgeImageView.image = UIImage(named: "Bookmarks/" + bookmark["type"]!)
    return cell
  }

}
