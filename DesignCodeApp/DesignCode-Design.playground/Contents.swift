//: A UIKit based Playground for presenting user interface

import UIKit
import PlaygroundSupport

class MyViewController : UIViewController {
  let backgroundImageView = UIImageView()
  let cardView = UIView()
  let titleLabel = UILabel()
  let captionLabel = UILabel()
  let coverImageView = UIImageView()
  let descriptionLabel = UILabel()
  let closeButton = UIButton(type: .system)

  override func loadView() {
    let view = UIView()
    view.backgroundColor = .white

    backgroundImageView.frame = .init(x: 0, y: 0, width: 375, height: 667)
    backgroundImageView.image = #imageLiteral(resourceName: "Chapters Screen@2x.png")
    view.addSubview(backgroundImageView)

    cardView.frame = .init(x: 20, y: 255, width: 300, height: 250)
    cardView.backgroundColor = .white
    cardView.layer.cornerRadius = 14
    cardView.layer.shadowOpacity = 0.25
    cardView.layer.shadowOffset = CGSize(width: 10, height: 10)
    cardView.layer.shadowRadius = 10
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleOpenCardView))
    cardView.addGestureRecognizer(tapGesture)

    //    label.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.frame = CGRect(x: 16, y: 16, width: 272, height: 38)
//    titleLabel.backgroundColor = .blue
    titleLabel.font = UIFont.systemFont(ofSize: 32, weight: .semibold)
    titleLabel.text = "Learn Swift 4"
    titleLabel.textColor = .white

    //    label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    //    label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true

    // 16 padding from left (x: 16) and then 300 width (from cardView) - 16 * 2 (left, right padding) = width of captionLabel
    captionLabel.frame = CGRect(x: 16, y: 204, width: 268, height: 40)
    captionLabel.backgroundColor = .clear
    captionLabel.text = "Design directly in playground"
    captionLabel.textColor = .white


//    coverImageView.frame = .init(x: 0, y: 0, width: 300, height: 250)
    coverImageView.frame = CGRect(x: 0, y: 0, width: 300, height: 250)
    coverImageView.image = #imageLiteral(resourceName: "Cover.jpg")
    coverImageView.contentMode = .scaleAspectFill
    coverImageView.clipsToBounds = true
    coverImageView.layer.cornerRadius = 14

//    cardView.addSubview(imageView)
//    cardView.insertSubview(coverImageView, belowSubview: captionLabel)

    view.addSubview(cardView)
    cardView.addSubview(coverImageView)
    cardView.addSubview(captionLabel)
    cardView.addSubview(titleLabel)

    descriptionLabel.frame = .init(x: 20, y: 448, width: 335, height: 132)
    descriptionLabel.numberOfLines = 0
    descriptionLabel.alpha = 0
    descriptionLabel.text = "Three years ago, Apple completely revamped their design language for the modern users. It is now much simpler, allowing designers to focus on animation and function rather than intricate visual details."
    descriptionLabel.textColor = .black

    cardView.addSubview(descriptionLabel)

    closeButton.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
    closeButton.frame = .init(x: 327, y: 20, width: 28, height: 28)
    closeButton.setImage(#imageLiteral(resourceName: "Action-Close@2x.png"), for: .normal)
    closeButton.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
    closeButton.layer.cornerRadius = 14
    closeButton.alpha = 0

    cardView.addSubview(closeButton)

    self.view = view
  }

  @objc func handleOpenCardView() {
    let animator = UIViewPropertyAnimator(duration: 0.5, dampingRatio: 0.7) {
      self.cardView.frame = CGRect(x: 0, y: 0, width: 375, height: 667)
      self.cardView.layer.cornerRadius = 0

      self.coverImageView.frame = .init(x: 0, y: 0, width: 375, height: 420)
      self.coverImageView.layer.cornerRadius = 0

      self.titleLabel.frame = .init(x: 20, y: 20, width: 375, height: 38)
      self.captionLabel.frame = .init(x: 20, y: 370, width: 335, height: 40)

      self.descriptionLabel.alpha = 1
      self.closeButton.alpha = 1

    }
    //    animator.startAnimation()
    animator.startAnimation(afterDelay: 2)
  }

  @objc func handleClose() {
    let animator = UIViewPropertyAnimator(duration: 0.5, dampingRatio: 0.7) {
      self.cardView.frame = CGRect(x: 20, y: 255, width: 300, height: 250)
      self.cardView.layer.cornerRadius = 14

      self.coverImageView.frame = CGRect(x: 0, y: 0, width: 300, height: 250)
      self.coverImageView.layer.cornerRadius = 14

      self.titleLabel.frame = CGRect(x: 16, y: 16, width: 268, height: 38)
      self.captionLabel.frame = CGRect(x: 16, y: 204, width: 268, height: 40)

      self.descriptionLabel.alpha = 0
      self.closeButton.alpha = 0
    }
    animator.startAnimation()
  }
}
// Present the view controller in the Live View window
PlaygroundPage.current.liveView = MyViewController()
