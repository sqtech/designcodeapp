//
//  MoreController.swift
//  DesignCodeApp
//
//  Created by Simon Quach on 2018-09-05.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class MoreController: UIViewController {
  struct SegueIdentifiers {
    static let moreToWeb = "MoreToWeb"
  }

  @IBAction func safariButtonTapped(_ sender: Any) {
    performSegue(withIdentifier: SegueIdentifiers.moreToWeb, sender: "https://designcode.io")
  }

  @IBAction func communityButtonTapped(_ sender: Any) {
    performSegue(withIdentifier: SegueIdentifiers.moreToWeb, sender: "https://spectrum.chat/design-code")
  }

  @IBAction func twitterHandleTapped(_ sender: Any) {
    performSegue(withIdentifier: SegueIdentifiers.moreToWeb, sender: "https://twitter.com/mengto")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  // MARK: Navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == SegueIdentifiers.moreToWeb {
      let navController = segue.destination as! UINavigationController
      let webController = navController.topViewController as! WebController
      webController.urlString = sender as! String
    }
  }
}
