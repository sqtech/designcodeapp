//
//  SectionCell.swift
//  DesignCodeApp
//
//  Created by Simon Quach on 2018-08-03.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class SectionCell: UICollectionViewCell {
  
  var section: [String: String]? {
    didSet {
      setupLayout()
    }
  }

  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var captionLabel: UILabel!
  @IBOutlet weak var coverImageView: UIImageView!
}

// MARK: Setup Methods
private extension SectionCell {
  func setupLayout() {
    titleLabel.text = section?["title"]
    captionLabel.text = section?["caption"]
    if let image = section?["image"] {
      coverImageView.image = UIImage(named: image)
    }
  }
}
