//
//  ChaptersController.swift
//  DesignCodeApp
//
//  Created by Simon Quach on 2018-08-12.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class ChaptersController: UIViewController {
  struct CellIdentifiers {
    static let sectionCell = "SectionCell"
  }

  @IBOutlet weak var chapter1CollectionView: UICollectionView!



  override func viewDidLoad() {
    super.viewDidLoad()
    chapter1CollectionView.dataSource = self
    chapter1CollectionView.delegate = self

    // Do any additional setup after loading the view.
  }

  /*
   // MARK: - Navigation

   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */

}

// MARK: - UIScrollViewDelegate
extension ChaptersController: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if let collectionView = scrollView as? UICollectionView {
      for cell in collectionView.visibleCells as! [SectionCell] {
        guard let indexPath = collectionView.indexPath(for: cell) else { return }
        guard let attributes = collectionView.layoutAttributesForItem(at: indexPath) else { continue }
        let cellFrame = collectionView.convert(attributes.frame, to: self.view)
        //        print(indexPath.item, cellFrame)
        // Testing speed from scrolling (lower is faster)
        //        let divideBy: CGFloat = indexPath.item % 2 == 0 ? 1 : 5
        let translationX = cellFrame.origin.x / 5
        cell.coverImageView.transform = CGAffineTransform(translationX: translationX, y: 0)

        cell.layer.transform = animateCell(with: cellFrame)
      }
    }

  }
}

// MARK: - UICollectionViewDataSource
extension ChaptersController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return sections.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.sectionCell, for: indexPath) as! SectionCell
    let section = sections[indexPath.item]
    cell.section = section
    cell.layer.transform = animateCell(with: cell.frame)
    return cell
  }
}

// MARK: - UICollectionViewDelegate
extension ChaptersController: UICollectionViewDelegate {

}
