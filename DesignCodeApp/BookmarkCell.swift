//
//  BookmarkCell.swift
//  DesignCodeApp
//
//  Created by Simon Quach on 2018-08-21.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class BookmarkCell: UITableViewCell {

  @IBOutlet weak var badgeImageView: UIImageView!
  @IBOutlet weak var chapterNumberLabel: UILabel!
  @IBOutlet weak var chapterTitleLabel: UILabel!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var bodyLabel: UILabel!
  
}
