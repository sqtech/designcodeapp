//
//  TestimonialCell.swift
//  DesignCodeApp
//
//  Created by Simon Quach on 2018-08-05.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class TestimonialCell: UICollectionViewCell {
  var testimonial: [String: String]? {
    didSet {
      setupLayout()
    }
  }

  @IBOutlet weak var textLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var jobLabel: UILabel!
  @IBOutlet weak var avatarImageView: UIImageView!

}

// MARK: Setup Methods
private extension TestimonialCell {
  func setupLayout() {
    textLabel.text = testimonial?["text"]
    nameLabel.text = testimonial?["name"]
    jobLabel.text = testimonial?["job"]
    if let image = testimonial?["avatar"] {
      avatarImageView.image = UIImage(named: image)
    }
  }
}
