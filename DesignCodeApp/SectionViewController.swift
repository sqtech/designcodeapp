//
//  SectionViewController.swift
//  DesignCodeApp
//
//  Created by Simon Quach on 2018-07-29.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class SectionViewController: UIViewController {
  // MARK: - Outlet Properties
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var captionLabel: UILabel!
  @IBOutlet weak var coverImageView: UIImageView!
  @IBOutlet weak var progressLabel: UILabel!
  @IBOutlet weak var bodyLabel: UILabel!

  // MARK: - IBAction Methods
  @IBAction func closeButtonTapped(_ sender: UIButton) {
    print("closeButtonTapped")
    dismiss(animated: true, completion: nil)
  }

  // MARK: Properties
  var sections: [[String: String]]!
  var section: [String: String]!
  var indexPath: IndexPath!

  override func viewDidLoad() {
    super.viewDidLoad()
    setupLayout()
  }

  override var prefersStatusBarHidden: Bool {
    return true
  }
}

private extension SectionViewController {
  func setupLayout() {
    titleLabel.text = section["title"]
    captionLabel.text = section["caption"]
    bodyLabel.text = section["body"]
    if let coverImage = section["image"] {
      coverImageView.image = UIImage(named: coverImage)
    }

    progressLabel.text = "\(indexPath.row + 1) / \(sections.count)"
  }
}
