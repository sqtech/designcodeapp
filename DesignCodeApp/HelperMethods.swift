//
//  HelperMethods.swift
//  DesignCodeApp
//
//  Created by Simon Quach on 2018-08-12.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

func animateCell(with cellFrame: CGRect) -> CATransform3D {
  // Ch.3 3D Animation
  // Change how the section scrolling looks

  // Rotates the cells inwards
  let angleFromX = Double((-cellFrame.origin.x) / 10)
  let angle = CGFloat((angleFromX * Double.pi) / 180)

  var transform = CATransform3DIdentity
  transform.m34 = -1 / 1000
  let rotation = CATransform3DRotate(transform, angle, 0, 1, 0)

  // Scale the size of cell (make it look big and small)
  // Set a limit
  // As the origin moves over to the left more, scaleFromX becomes closer to 1 (normal size looking cell)
  var scaleFromX = (1000 - (cellFrame.origin.x - 200)) / 1000
  let scaleMax: CGFloat = 1
  let scaleMin: CGFloat = 0.6
  if scaleFromX > scaleMax {
    scaleFromX = scaleMax
  }
  if scaleFromX < scaleMin {
    scaleFromX = scaleMin
  }

  let scale = CATransform3DScale(CATransform3DIdentity, scaleFromX, scaleFromX, 1)

  return CATransform3DConcat(rotation, scale)
}
